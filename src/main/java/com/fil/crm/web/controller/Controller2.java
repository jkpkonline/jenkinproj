package com.fil.crm.web.controller;

import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fil.crm.web.constants.RequestMappingConstants;
import com.fil.crm.web.domain.GetMeetingsResponse;
import com.fil.crm.web.helper.DateHelper;
import com.fil.crm.web.helper.DateHelper.DateRange;
import com.fil.crm.web.helper.RantNRaveClient;
import com.fil.crm.web.helper.SFDCHelper;

@Controller
public class Controller2 {

	private final Logger logger = LoggerFactory.getLogger(Controller2.class);

	@RequestMapping(value = "/welcome11", method = { RequestMethod.GET, RequestMethod.POST })
	public String welcome(
			@RequestParam(value = RequestMappingConstants.NAME_PARAM, required = false) String name,
			@RequestParam(value = RequestMappingConstants.USERNAME_PARAM, required = false) String username,
			Model model) {
		model.addAttribute("name", name);
		model.addAttribute("username", username);
		model.addAttribute("name", name);
		logger.info("Controller2 -- Welcome");
		return "myMeetings";
	}

	@RequestMapping(value = "/error11", method = { RequestMethod.GET })
	public String error() {
		System.out.println("I am Controller2:/error-----");
		return "error";
	}

	@RequestMapping(value = "/aa", method = { RequestMethod.GET })
	public ModelAndView createViewMeeting() {
		System.out.println("I am Controller2:/home ");
		return new ModelAndView("redirect:/mymeetings?range=TODAY");
	}

	@RequestMapping(value = "/meetingnotes11", method = { RequestMethod.GET })
	public String meetingnotes(@RequestParam String eventId, ModelMap map) {
		System.out.println("meeting notes page:/");
		GetMeetingsResponse response = SFDCHelper.getMeetingDetailsFromSFDC(eventId);
		map.addAttribute("meetingDetails", response.getMeetings().get(0));
		return "meetingNotes";
	}

	@RequestMapping(value = "/people11", method = { RequestMethod.GET })
	public String people() {
		System.out.println("meeting notes page:/  ");
		return "people";
	}

	@RequestMapping(value = "/feed11", method = { RequestMethod.GET })
	public String feed() {
		System.out.println("meeting notes page:/");
		return "feed";
	}

	@RequestMapping(value = "getMeetings11", method = RequestMethod.GET)
	public @ResponseBody GetMeetingsResponse getMeetings(@RequestParam String dateFrom,
			@RequestParam String dateTo) {
		return SFDCHelper.getMeetingsFromSFDC(dateFrom, dateTo);
	}

	@RequestMapping(value = "/collaboration11", method = { RequestMethod.GET })
	public String collaboration() {
		System.out.println("I am Controller2:/collaboration");
		return "collaboration";
	}

	@RequestMapping(value = "/dashboards11", method = { RequestMethod.GET })
	public String dashboards() {
		System.out.println("I am Controller2:/dashboards");
		return "dashboards";
	}

	@RequestMapping(value = "/home11", method = { RequestMethod.GET })
	public String home() {
		System.out.println("Home page:/");
		return "home";
	}

	@RequestMapping(value = "/mymeetings11", method = { RequestMethod.GET })
	public String myMeetings(ModelMap map, @RequestParam DateRange range) {
		List<String> rangeList;
		try {
			rangeList = DateHelper.getDateRange(range);
			GetMeetingsResponse response = SFDCHelper.getMeetingsFromSFDC(rangeList.get(0), rangeList.get(1));
			map.addAttribute("meetingList", "");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return "myMeetings";
	}

	@RequestMapping(value = "/sendSurvey11", method = { RequestMethod.GET })
	public String sendSurvey(@RequestParam String eventId, ModelMap map) {
		GetMeetingsResponse response = SFDCHelper.getMeetingDetailsFromSFDC(eventId);
		map.addAttribute("meetingDetails", response.getMeetings().get(0));
		return "sendSurvey";
	}

	@RequestMapping(value = "/collaboration-myteam11", method = { RequestMethod.GET })
	public String collaborationMyteam() {
		return "collaboration-myteam";
	}

	@RequestMapping(value = "/collaboration-product11", method = { RequestMethod.GET })
	public String collaborationProduct() {
		return "collaboration-product";
	}

	@RequestMapping(value = "/collaboration-rfp11", method = { RequestMethod.GET })
	public String collaborationRfp() {
		return "collaboration-rfp";
	}

	@RequestMapping(value = "/stfw11", method = { RequestMethod.GET })
	public String stfw() {
		return "stfw";
	}

	@RequestMapping(value = "/accounts11", method = { RequestMethod.GET })
	public String accounts() {
		return "accounts";
	}

	@RequestMapping(value = "/sendRnREmail11", method = { RequestMethod.GET })
	public @ResponseBody String sendRnREmail() {
		return RantNRaveClient.sendSurveyEmail();
	}

	@RequestMapping(value = "/mySurveys11", method = { RequestMethod.GET })
	public String mySurveys(ModelMap map) {
		return "mySurveys";
	}

	@RequestMapping(value = "/opportunities11", method = { RequestMethod.GET })
	public String opportunities() {
		return "opportunities";
	}

	@RequestMapping(value = "/thoughtLeadership11", method = { RequestMethod.GET })
	public String thoughtLeadership() {
		return "thoughtLeadership";
	}

	@RequestMapping(value = "/opportunitiesDrilldown11", method = { RequestMethod.GET })
	public String opportunitiesDrilldown() {
		return "opportunitiesDrilldown";
	}

	@RequestMapping(value = "/infoAnalyticsProduct11", method = { RequestMethod.GET })
	public String infoAnalyticsProduct() {
		return "infoAnalyticsProduct";
	}

}
